up:
	touch .env
	docker-compose up -d
	@echo "App is running on http://localhost:8091"

stop:
	docker-compose stop

clean:
	docker-compose down