FROM php:7.4-apache-buster

RUN apt-get update && apt-get install -y zlib1g-dev git libpq-dev libzip-dev unzip

ADD docker/virtual_host_prod.conf /etc/apache2/sites-available/000-default.conf
RUN a2enmod rewrite && a2enmod headers && a2enmod expires

# opcache
RUN docker-php-ext-install opcache
ADD docker/opcache.ini /usr/local/etc/php/conf.d/opcache.ini

# composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php composer-setup.php --install-dir=/bin --filename=composer \
    && php -r "unlink('composer-setup.php');"

ARG COMMIT_SHA
ENV COMMIT_SHA ${COMMIT_SHA}

WORKDIR /var/www/html

ADD Dockerfile /Dockerfile
ADD . /var/www/html

RUN composer install --no-dev --optimize-autoloader && php -v