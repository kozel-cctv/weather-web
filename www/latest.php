<?php

use Aws\S3\S3Client;

$secret = (int)getenv('CODE');
$code = (int)$_COOKIE['code'];
if ($code !== $secret) {
    http_response_code(401);
    die;
}

require __DIR__ . '/../vendor/autoload.php';

$s3 = new S3Client([
    'credentials' => new Aws\Credentials\Credentials(
        \getenv('AWS_ACCESS_KEY_ID'),
        \getenv('AWS_SECRET_ACCESS_KEY')
    ),
    'region' => \getenv('AWS_REGION'),
    'version' => 'latest',
]);

$file = $s3->getObject([
    "Bucket" => \getenv('AWS_S3_BUCKET'),
    "Key" => \getenv('USER_ACCOUNT') . "/the-newest.jpg"
]);


\header('Content-type: image/jpg');
echo($file['Body']);
die;

