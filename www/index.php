<?php require __DIR__ . '/../app/header.php'; ?>

<div class="container">
    <div class="row ">
        <div class="col-md-12">
            <h1><?php echo \ucfirst(\getenv('USER_ACCOUNT')); ?> CCTV</h1>
            <img src="latest.php" alt="" width="100%">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php
            $iterator = (new \DateTime('now'));
            $t = (int)\getenv('FIRST_DAY');

            while ((int)$iterator->format('Ymd') > $t) {
                echo "<a href=\"/list.php?day=" . $iterator->format('Ymd') . '">' . $iterator->format('Y-m-d') . "</a>&nbsp;";
                $iterator->modify('-1 day');
            }
            ?>
        </div>
    </div>
</div>

<?php require __DIR__ . '/../app/footer.php'; ?>