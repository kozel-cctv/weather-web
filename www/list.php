<?php require __DIR__ . '/../app/header.php'; ?>

    <body>
<div class="container disable-dbl-tap-zoom">
    <div class="row">
        <div class="col-md-12">
            <?php

            use Aws\S3\S3Client;

            require __DIR__ . '/../vendor/autoload.php';
            $reg = "/[a-z]+\/\d+\/P(\d+)\.jpg/"; // parse date from image name

            $day = (int)$_GET['day'];

            $s3 = new S3Client([
                'credentials' => new Aws\Credentials\Credentials(
                    \getenv('AWS_ACCESS_KEY_ID'),
                    \getenv('AWS_SECRET_ACCESS_KEY')
                ),
                'region' => \getenv('AWS_REGION'),
                'version' => 'latest',
            ]);

            $iterator = $s3->getIterator("ListObjects", [
                "Bucket" => \getenv('AWS_S3_BUCKET'),
                "Prefix" => \getenv('USER_ACCOUNT') . "/$day/",
            ]);

            $objects = [];
            foreach ($iterator as $object) {
                preg_match($reg, $object['Key'], $m);
                $objects[] = [
                    'image' => '/photo.php?path=' . $object["Key"],
                    'time' => \DateTime::createFromFormat('ymdHis', floor($m[1] / 100))->format('H:i'),
                ];
            }


            $first = null;
            if (count($objects) > 0) {
                $first = $objects[0];
            }

            ?>


            <img src="" alt="" id="main_img">
            <p class="timer">
                <span id="time">unknown</span>&nbsp;
                <span id="progress"></span>&nbsp;
                <span class="speed" id="speed1" style="display: none">1x</span>&nbsp;
                <span class="speed" id="speed2" style="display: none">2x</span>&nbsp;
                <span class="speed" id="speed3" style="display: none">3x</span>
            </p>
            <p>
                <img id="icon" class="icon" src="/img/play.png" alt="">
                <input type="range" class="custom-range my-range" id="range" step="1" min="0" value="0"
                       max="<?php echo count($objects) - 1; ?>">
            </p>
        </div>
    </div>
</div>

<script>
    let data = JSON.parse('<?php echo \json_encode($objects); ?>');
</script>
<script src="/js/list.js?v=<?php echo \getenv('COMMIT_SHA')?>"></script>

<?php require __DIR__ . '/../app/footer.php'; ?>