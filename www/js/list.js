let images = [];
let img = document.getElementById("main_img");
let range = document.getElementById("range");
let time = document.getElementById("time");
let icon = document.getElementById("icon");
let progress = document.getElementById("progress");
let speed = 500;
let s1 = document.getElementById("speed1");
let s2 = document.getElementById("speed2");
let s3 = document.getElementById("speed3");
let current = 0; // current index
let loaded = 0; // number of loaded images
let playing = false;
let interval = false;

function preloadImage(url) {
    let img = new Image();
    img.src = url;
    let loadedFunc = function () {
        loaded++;
        let prg = Math.round((loaded / data.length) * 100);
        if (prg >= 100) {
            progress.innerText = "";
        } else {
            progress.innerText = " - (" + prg + " %)";
        }
        if (loaded === data.length) {
            // enable input event when all images loaded
            icon.src = "/img/pause.png";
            current = 0;
            range.value = 0;
            togglePlay();
            range.addEventListener("input", change);
        }
    };
    img.onload = loadedFunc;
    img.onerror = loadedFunc;
    return img;
}

function togglePlay() {
    displaySpeed();
    if (playing) {
        playing = false;
        icon.src = "/img/play.png";
        clearInterval(interval);
    } else if (loaded < data.length) {
        icon.src = "/img/spinner.gif";
        for (let i = 0; i < data.length; i++) {
            if (data[i] && !images[i]) {
                images[i] = preloadImage(data[i].image);
            }
        }
    } else {
        if (current === data.length - 1) {
            range.value = 0;
            change();
        }
        playing = true;
        icon.src = "/img/pause.png";
        interval = setInterval(function () {
            if (!data[current + 1]) {
                togglePlay();
                return;
            }
            current++;
            time.innerText = data[current].time;
            range.value = current;
            change();
        }, speed);
    }
}

function change() {
    current = parseInt(range.value);
    if (data[current]) {
        if (!images[current]) {
            images[current] = preloadImage(data[current].image)
        }
        img.src = images[current].src;
    }

    return false;
}

function displaySpeed() {
    s1.style.display = 'inline';
    s2.style.display = 'inline';
    s3.style.display = 'inline';
}

function normalizeSpeed() {
    s1.style.fontWeight = 'normal';
    s2.style.fontWeight = 'normal';
    s3.style.fontWeight = 'normal';
}

function speedListener(el, speedValue) {
    el.addEventListener("click", function (){
        speed = speedValue;
        normalizeSpeed();
        // pause and unpause to reflect speed valud
        togglePlay();
        togglePlay();
        el.style.fontWeight = 'bold';
        return false;
    });
}


// load first image
images[current] = preloadImage(data[current].image);
img.src = images[current].src;
time.innerText = data[current].time;
preloadImage("/img/pause.png");
preloadImage("/img/play.png");
preloadImage("/img/spinner.gif");
s1.style.fontWeight = 'bold'; // bold default speed value
speedListener(s1, 500); // 500 ms between img - default value
speedListener(s2, 200); // 200 ms between img
speedListener(s3, 100); // 100 ms between img

// update timer
range.addEventListener("input", function () {
    current = parseInt(range.value);
    time.innerText = data[current].time;
});

// change image
range.addEventListener("change", change);

// load all images
icon.addEventListener("click", function () {
    togglePlay();
    return false;
});