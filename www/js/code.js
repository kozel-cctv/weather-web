let code = [];
let codeEl = document.getElementById("code");

for (let i = 1; i < 10; i++) {
    document.getElementById("num"+i).addEventListener('click', function () {
        code.push(i);
        codeEl.innerText = code.join("");
        if (code.length === 4) {
            document.cookie = "code=" + code.join("");
            location.reload();
        }
        return false;
    });
}
