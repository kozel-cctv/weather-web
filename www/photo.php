<?php

use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;

$secret = (int)getenv('CODE');
$code = (int)$_COOKIE['code'];
if ($code !== $secret) {
    http_response_code(401);
    die;
}

$path = $_GET['path'];
if (empty($path)) {
    http_response_code(404);
    die;
}

require __DIR__ . '/../vendor/autoload.php';

$s3 = new S3Client([
    'credentials' => new Aws\Credentials\Credentials(
        \getenv('AWS_ACCESS_KEY_ID'),
        \getenv('AWS_SECRET_ACCESS_KEY')
    ),
    'region' => \getenv('AWS_REGION'),
    'version' => 'latest',
]);

try {
    $file = $s3->getObject([
        "Bucket" => \getenv('AWS_S3_BUCKET'),
        "Key" => $path,
    ]);
} catch (S3Exception $exception) {
    http_response_code(404);
    die;
}


\header('Content-type: image/jpg');
\header('Pragma: public');
\header('Cache-Control: max-age=86400');
header('Expires: ' . gmdate('D, d M Y H:i:s \G\M\T', time() + 86400));
echo($file['Body']);
die;
