<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo \ucfirst(\getenv('USER_ACCOUNT')); ?> cctv</title>
    <link rel="stylesheet" href="/css/bootstrap.css?v=<?php echo \getenv('COMMIT_SHA')?>">
    <link rel="stylesheet" href="/css/style.css?v=<?php echo \getenv('COMMIT_SHA')?>">
</head>

<body>

<?php
$secret = (int)getenv('CODE');
$code = (int)$_COOKIE['code'];
if ($code !== $secret) {
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3 id="code">KOD</h3>
            <?php for ($i = 1; $i < 10; $i++) {
                echo
                "<a class='btn-warning btn btn-lg num' data-val='{$i}' id='num{$i}'>
                    {$i}
                </a>";
                if ($i % 3 === 0) echo "<br>";
            } ?>
        </div>
    </div>
</div>
<script src="/js/code.js?v=<?php echo \getenv('COMMIT_SHA')?>"></script>
</body>
</html>
<?php
die;
}
?>