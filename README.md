# Kozel CCTV viewer

Application displays the latest available image.


## Configuration env variables:

USER_ACCOUNT=kozel
FIRST_DAY=20191124
AWS_REGION=eu-west-1
AWS_ACCESS_KEY_ID=secret
AWS_SECRET_ACCESS_KEY=secret
AWS_S3_BUCKET=secret
CODE=1234
COMMIT_SHA=abcd